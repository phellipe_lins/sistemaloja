package br.controle.estoque.servlets.produto;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.controle.estoque.classe.Categoria;
import br.controle.estoque.classe.Produto;
import br.controle.estoque.dao.ProdutoDao;

/**
 * Servlet implementation class InserirProduto
 */
@WebServlet("/InserirProduto")
public class InserirProduto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InserirProduto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProdutoDao dao = new ProdutoDao();
		Produto produto =  new Produto();
		Categoria categoria = new Categoria();
		
		String nomeProduto = request.getParameter("nomeProduto");
		String descricaoProduto = request.getParameter("descricaoProduto");
		String valorString = request.getParameter("valorProduto");
		String qtdeString = request.getParameter("qtdeProduto");
		String categoria_id = request.getParameter("idCategoria");
		double valorProduto = Double.parseDouble(valorString);
		int qtdeProduto = Integer.parseInt(qtdeString);
		int idCategoria = Integer.parseInt(categoria_id);
		
		produto.setDescricaoProduto(descricaoProduto);
		produto.setNomeProduto(nomeProduto);
		produto.setQtdeProduto(qtdeProduto);
		produto.setValorProduto(valorProduto);
		categoria.setId(idCategoria);
		produto.setCategoria(categoria);
		
		dao.incluir(produto);
		
		response.sendRedirect("/SistemaLoja/produto/index.jsp");
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
