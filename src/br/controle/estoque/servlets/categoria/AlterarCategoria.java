package br.controle.estoque.servlets.categoria;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.controle.estoque.classe.Categoria;
import br.controle.estoque.dao.CategoriaDao;

/**
 * Servlet implementation class AlterarCategoria
 */
@WebServlet("/AlterarCategoria")
public class AlterarCategoria extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AlterarCategoria() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CategoriaDao dao = new CategoriaDao();
		Categoria c = new Categoria();
		String nomeCategoria =  request.getParameter("nomeCategoria");
		int id = Integer.parseInt(request.getParameter("idCategoria"));
		c.setNomeCategoria(nomeCategoria);
		c.setId(id);
		dao.alterar(c);
		response.sendRedirect("/SistemaLoja/ListarCategoria");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
