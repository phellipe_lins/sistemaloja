package br.controle.estoque.servlets.categoria;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.controle.estoque.classe.Categoria;
import br.controle.estoque.dao.CategoriaDao;

/**
 * Servlet implementation class CadastrarCategoria
 */
@WebServlet("/CadastrarCategoria.do")
public class CadastrarCategoria extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CadastrarCategoria() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CategoriaDao dao = new CategoriaDao();
		Categoria categoria = new Categoria();
		
		String nome = request.getParameter("nomeCategoria");
		categoria.setNomeCategoria(nome);
		dao.inserir(categoria);
		response.sendRedirect("/SistemaLoja/categoria/index.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

}
