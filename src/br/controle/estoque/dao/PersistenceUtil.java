package br.controle.estoque.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceUtil {

	private static EntityManagerFactory emf;
	
	public static EntityManager getEntityManager(){
		if (emf == null) {
			emf = Persistence.createEntityManagerFactory("controlestoque");
		}
		return emf.createEntityManager();
		
	}
}
