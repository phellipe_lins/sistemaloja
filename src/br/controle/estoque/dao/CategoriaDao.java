package br.controle.estoque.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;

import br.controle.estoque.classe.Categoria;

public class CategoriaDao {

	public void inserir(Categoria categoria){
		EntityManager em = PersistenceUtil.getEntityManager();
		
		em.getTransaction().begin();
		em.persist(categoria);
		em.getTransaction().commit();
		em.close();
	}
	
	public void alterar(Categoria categoria){
		EntityManager em = PersistenceUtil.getEntityManager();
		
		em.getTransaction().begin();
		em.merge(categoria);
		em.getTransaction().commit();
		em.close();
	}
	
	public Categoria findById(int id){
		EntityManager em = PersistenceUtil.getEntityManager();
		Categoria c = em.find(Categoria.class, id);
		if(c == null){
			throw new EntityNotFoundException("N�o foi Possivel encontrar pelo ID " + id);
		}
		return c;
	}
	
	public void excluir(int id){
		
		EntityManager em = PersistenceUtil.getEntityManager();
		Categoria c = findById(id);
		em.getTransaction().begin();
		em.remove(em.merge(c));
		em.getTransaction().commit();
		em.close();
	}
	
	public List<Categoria> lista(){
		
		String jpql = "select c from Categoria c";
		EntityManager em = PersistenceUtil.getEntityManager();
		Query query = em.createQuery(jpql);
		List<Categoria> listCategoria = query.getResultList();
		
		return listCategoria;
	}
	
	public List<Categoria> listaById(int id){
		EntityManager em = PersistenceUtil.getEntityManager();
		String jpql = "select c from Categoria c WHERE id = "+id;
		Query query = em.createQuery(jpql);
		List<Categoria> listId = query.getResultList();
		return listId;
	}
}
