package br.controle.estoque.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.controle.estoque.classe.Produto;

public class ProdutoDao {

	public void incluir(Produto produto){
		EntityManager em = PersistenceUtil.getEntityManager();
		em.getTransaction().begin();
		em.persist(produto);
		em.getTransaction().commit();
		em.close();
	}
	
	public void alterar(Produto produto){
		EntityManager em = PersistenceUtil.getEntityManager();
		
		em.getTransaction().begin();
		em.merge(produto);
		em.getTransaction().commit();
		em.close();
	}
	
	public Produto findById(int id){
		EntityManager em = PersistenceUtil.getEntityManager();
		Produto p = em.find(Produto.class, id);
		return p;
	}
	
	public void excluir(int id){
		EntityManager em = PersistenceUtil.getEntityManager();
		Produto p = findById(id);
		em.getTransaction().begin();
		em.remove(em.merge(p));
		em.getTransaction().commit();
		em.close();
	}
	
	public List<Produto> listar(){
		
		String jpql = "select p from Produto p";
		
		EntityManager em = PersistenceUtil.getEntityManager();
		Query query = em.createQuery(jpql);
		List<Produto> listProduto = query.getResultList();
		
		return listProduto;
	}
}
