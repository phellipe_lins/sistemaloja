package br.controle.estoque.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.controle.estoque.classe.Cliente;

public class ClienteDao {
	
	public void incluir(Cliente cliente){
		EntityManager em = PersistenceUtil.getEntityManager();
		em.getTransaction().begin();
		em.persist(cliente);
		em.getTransaction().commit();
		em.close();
	}
	
	public void alterar(Cliente cliente){
		EntityManager em = PersistenceUtil.getEntityManager();
		
		em.getTransaction().begin();
		em.merge(cliente);
		em.getTransaction().commit();
		em.close();
	}
	
	public Cliente findById(int id){
		EntityManager em = PersistenceUtil.getEntityManager();
		Cliente c = em.find(Cliente.class, id);
		return c;
	}
	
	public void excluir(int id){
		EntityManager em = PersistenceUtil.getEntityManager();
		Cliente c = findById(id);
		em.getTransaction().begin();
		em.remove(em.merge(c));
		em.getTransaction().commit();
		em.close();
	}
	
	public List<Cliente> listar(){
		
		String jpql = "select c from Cliente c";
		
		EntityManager em = PersistenceUtil.getEntityManager();
		Query query = em.createQuery(jpql);
		List<Cliente> listCliente = query.getResultList();
		
		return listCliente;
	}

}
