package br.controle.estoque.classe;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ConnectionTest {
	
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("controlestoque");
		EntityManager em = emf.createEntityManager();
		em.close();
		emf.close();
		System.out.println("Tudo certo");
	}

}
