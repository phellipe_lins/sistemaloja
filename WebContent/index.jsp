<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="header.jsp" />

<h1>Vendas</h1>

<form action="vender" method="POST">
	<table>
		<thead>
			<tr>
				<th>Produto</th>
				<th>Valor</th>
				<th>Estoque (Und)</th>
				<th>Quantidade</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${listaProduto}" var="lista">
			<tr>
				<td>${ lista.nomeProduto }</td>
				<td>R$ ${ lista.valorProduto }</td>
				<td>${ lista.qtdeProduto }</td>
				<td>
					<input type="number" name="quantidade">
					<input type="text" name="id" value="${ lista.id }">
				</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>
	<p><button type="submit">Concluir Venda</button></p>
</form>

<c:import url="footer.jsp" />