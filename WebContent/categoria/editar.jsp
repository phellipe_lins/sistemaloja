<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="../header.jsp" />

	<form action="/SistemaLoja/AlterarCategoria" method="post">
		<c:forEach items="${listaId }" var="lista">
			<input type="text" name="nomeCategoria" value="${lista.nomeCategoria }"/>
			<input type="hidden" name="idCategoria" value="${lista.id }" />
			<input type="submit" value="Enviar" />
		</c:forEach>
	</form>

<c:import url="../footer.jsp" />