<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="../header.jsp" />

	<table>
		<thead>	
			<tr>
				<th>Categoria</th>
				<th>Excluir</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${listaCategoria }" var="lista">
			<tr>
				<td>${lista.nomeCategoria }</td>
				<td><a href="/SistemaLoja/ExcluirCategoria?id=${lista.id}">Excluir</a></td>
			</tr>
			</c:forEach>
		</tbody>
	</table>

<c:import url="../footer.jsp" />