<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="../header.jsp" />

	<form action="/SistemaLoja/InserirProduto" method="POST">
		<label for="nomeProduto">Produto:</label><br>
		<input type="text" name="nomeProduto"/><br>
		
		<label for="descricaoProduto">Descric�o:</label><br>
		<textarea name="descricaoProduto"></textarea><br>
		
		<label for="valorProduto">Valor:</label><br>
		<input type="text" name="valorProduto"/><br>
		
		<label for="qtdeProduto">Quantidade Dispon�vel:</label><br>
		<input type="text" name="qtdeProduto"/><br>
		<select name="idCategoria">
			<c:forEach items="${categorias}" var="lista">
				<option value="${lista.id}">${lista.nomeCategoria}</option>
			</c:forEach>
		</select>
		<input type="submit" value="enviar">
	</form>

<c:import url="../footer.jsp" />