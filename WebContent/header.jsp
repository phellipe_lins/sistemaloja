<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sistema Loja</title>
<style>
	body, html {
		margin: 0;
		padding: 0;
		height: 100%;
	}
	main {
		min-height: 100%;
		position: relative;
	}
	section {
		padding-bottom: 50px;
		margin-left: auto;
		margin-right: auto;
		padding-top: 50px;
		max-width: 768px;
	}
	footer {
		position: absolute;
		bottom: 0;
		left: 0;
		width: 100%;
		height: 50px;
		line-height: 50px;
		text-align: center;
		background-color: #c3c3c3;
	}
	nav {
		display: block;
		text-align: center;
		background-color: #c3c3c3;
		margin: 0;
	}
	nav a {
		display: inline-block;
		color: #333333;
		font-size: 16px;
		text-decoration: none;
		text-transform: uppercase;
		margin: 0 5px;
		line-height: 50px;
	}
	table {
		width: 100%;
	}
	table th {
		border: 1px solid #333333;
	}
	table td {
		border: 1px solid #333333;
	}
</style>
</head>
<body>
	<main>
		<nav>
			<a href="/SistemaLoja/ListarCategoria">Listar Categorias</a>
			<a href="/SistemaLoja/categoria/CadastrarCategoria.jsp">Cadastrar Categoria</a>
			<a href="/SistemaLoja/ListarProduto">Listar Produtos</a>
			<a href="/SistemaLoja/CadastrarProduto">Cadastrar Produto</a>
		</nav>
		<section>
